FROM alpine

RUN apk add --no-cache curl openldap-clients coreutils

COPY ./backup ./clean /etc/periodic/daily/

CMD crond -fL8
